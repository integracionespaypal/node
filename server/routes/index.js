const express = require('express');

const app = express();

app.use(require('../modules/expressCheckout/routes.expressCheckout'));
app.use(require('../modules/braintree/routes.braintree'));

module.exports = app;