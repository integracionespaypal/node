const express = require("express");

const _ = require("underscore");
let braintree = require("braintree");

const gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: process.env.merchantId,
  publicKey: process.env.publicKey,
  privateKey: process.env.privateKey,
});

exports.generate_client_token = (req, res) => {
  gateway.clientToken.generate({}, function (err, response) {
    res.json({
      result: response.clientToken,
    });
  });
};
exports.execute_payment = (req, res) => {

  let nonceFromTheClient = req.body.nonce;
  try {
    gateway.transaction.sale(
      {
        amount: "142",
        paymentMethodNonce: nonceFromTheClient,
        options: {
          submitForSettlement: true,
        },
      },
      function (err, result) {
        if(err){
          return res.status(400).json({
            ok: false,
            err
          });
        }
        console.log(result)

        if (result.success == false){
          return res.status(402).json({
            ok: false,
            result
          });
        }
        
        res.json({
          ok: true,
          result
        });
      }
    );
  } catch (error) {
    res.status(400).json({
      ok: false,
      error
    });
  }
  
};


exports.get_all_transactions = (req, res)=>{
  // console.log("Gor here");
  gateway.transaction.search(function (search) {
    search.amount().min("1");
    }, function (err, response) {
      if (err){
        console.log(err);
        return res.status(400).json({
          ok: false,
          err
        });
      }
      let transactions = [];
      let sendBack = response.ids.length;

      response.ids.forEach(transactionId => {
        gateway.transaction.find(transactionId, function (err, transaction) {
          if(err){
            console.log("Error");
            return res.status(400).json({
              ok: false,
              err
            });
          }
          // console.log(transaction);
          if (transaction.type != "credit"){
            let tmpTransaction = {
              id: transaction.id,
              status: transaction.refundId ? "Refunded":transaction.status,
              currencyIsoCode: transaction.currencyIsoCode,
              amount: transaction.amount,
              createdAt: transaction.createdAt
            }
            transactions.push(tmpTransaction);
          }
          
          // console.log(transactions)
          // console.log(sendBack);
          if (sendBack == 1){
            res.json(
              {
                ok: true,
                transactions
              }
            )
          }
          sendBack--;
          
        });        
      });
    });
}

exports.refund_transaction = (req, res)=>{

  console.log(req.body);
  let transactionId = req.body.id;
  gateway.transaction.refund(transactionId, function (err, result) {
    if (err){
      return res.status(400).json({
        ok: false,
        err
      });
    }
    res.json(
      {
        ok: true,
        result
      }
    )
  });
}