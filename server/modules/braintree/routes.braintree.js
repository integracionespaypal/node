const express = require('express');
const btController = require('./controller.braintree');

const app = express();

app.get('/api/generate-client-token/', btController.generate_client_token);
app.post('/api/process-transaction/', btController.execute_payment);
app.get('/api/get-all-transactions/', btController.get_all_transactions);
app.post('/api/refund-transaction',btController.refund_transaction);

module.exports = app;