const express = require('express');
const ecController = require('./controller.expressCheckout');

const app = express();

app.post('/api/create-payment/', ecController.create_payment);
app.post('/api/execute-payment/', ecController.execute_payment);

module.exports = app;