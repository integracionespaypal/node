const express = require('express');

const _ = require('underscore');
const paypal = require('@paypal/checkout-server-sdk');


let clientId = process.env.CLIENT
let clientSecret = process.env.SECRET;
let environment = new paypal.core.SandboxEnvironment(clientId, clientSecret);
let client = new paypal.core.PayPalHttpClient(environment);

exports.create_payment = async (req, res) => {
  // console.log('got here');
  // console.log(process.env.SECRET);
  
  let request = new paypal.orders.OrdersCreateRequest();
  request.requestBody({
    "intent": "CAPTURE",
    "application_context": {
      "return_url": "https://example.com",
      "cancel_url": "https://example.com",
      "brand_name": "EXAMPLE INC",
      "locale": "en-US",
      "landing_page": "BILLING",
      "shipping_preference": "SET_PROVIDED_ADDRESS",
      "user_action": "CONTINUE"
    },
    "purchase_units": [
      {
        "reference_id": "PUHF",
        "description": "Sporting Goods",
  
        "custom_id": "CUST-HighFashions",
        "soft_descriptor": "HighFashions",
        "amount": {
          "currency_code": "EUR",
          "value": "230.00",
          "breakdown": {
            "item_total": {
              "currency_code": "EUR",
              "value": "180.00"
            },
            "shipping": {
              "currency_code": "EUR",
              "value": "30.00"
            },
            "handling": {
              "currency_code": "EUR",
              "value": "10.00"
            },
            "tax_total": {
              "currency_code": "EUR",
              "value": "20.00"
            },
            "shipping_discount": {
              "currency_code": "EUR",
              "value": "10"
            }
          }
        },
        "items": [
          {
            "name": "T-Shirt",
            "description": "Green XL",
            "sku": "sku01",
            "unit_amount": {
              "currency_code": "EUR",
              "value": "90.00"
            },
            "tax": {
              "currency_code": "EUR",
              "value": "10.00"
            },
            "quantity": "1",
            "category": "PHYSICAL_GOODS"
          },
          {
            "name": "Shoes",
            "description": "Running, Size 10.5",
            "sku": "sku02",
            "unit_amount": {
              "currency_code": "EUR",
              "value": "45.00"
            },
            "tax": {
              "currency_code": "EUR",
              "value": "5.00"
            },
            "quantity": "2",
            "category": "PHYSICAL_GOODS"
          }
        ],
        "shipping": {
          "method": "United States Postal Service",
          "address": {
            "name": {
              "full_name":"John",
              "surname":"Doe"
            },
            "address_line_1": "123 Townsend St",
            "address_line_2": "Floor 6",
            "admin_area_2": "San Francisco",
            "admin_area_1": "CA",
            "postal_code": "94107",
            "country_code": "US"
          }
        }
      }
    ]
  });

  let createOrder  = async function(){
    let response = await client.execute(request)
    
    // console.log(`Response: ${JSON.stringify(response)}`);
    // // If call returns body in response, you can get the deserialized version from the result attribute of the response.
    // console.log(`Order: ${JSON.stringify(response.result)}`);
    return response.result;    


  }

  let PPResponse = await createOrder();
  

  res.json({
    result: PPResponse
  });
}

exports.execute_payment = async (req, res) => {

  // let body = _.pick(req.body, [
  //   'orderId',
  //   'payerId'
  // ]);

  console.log(req.body.orderID)

  let captureOrder =  async function(orderId) {
    console.log('here 2')
    request = new paypal.orders.OrdersCaptureRequest(orderId);
    request.requestBody({});
    // Call API with your client and get a response for your call
    let response = await client.execute(request);
    // console.log(`Response: ${JSON.stringify(response)}`);
    // // If call returns body in response, you can get the deserialized version from the result attribute of the response.
    // console.log(`Capture: ${JSON.stringify(response.result)}`);

    return response.result;
  }

  let capture = await captureOrder(req.body.orderID);

  console.log(`Capture: ${JSON.stringify(capture)}`);

  res.json({
    result: capture
  });
}
