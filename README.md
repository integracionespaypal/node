## Istalling node packages

npm install

## Fixing vulnerabilities in libraries

npm audit fix

## Staring the server

nodemon server/server.js

### or 

npm start server/server.js